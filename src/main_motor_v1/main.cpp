#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "wiringPi/wiringPi.h"
#include "wiringPi/softServo.h"

int main() {
    if (wiringPiSetupGpio() == -1) {
        fprintf(stdout, "oops: %s\n", strerror(errno));
        return 1;
    }

    softServoSetup(17, -1, -1, -1, -1, -1, -1, -1);
    softServoWrite(17, 0);

    while (true) {
        int value;
        scanf("%d", &value);
        softServoWrite(17,  value);
    }
}