#include <iostream>
#include <cstdio>
#include <ctime>
#include <cmath>
#include <sys/ioctl.h>

#include "mpu6050.h"

uint64_t get_time_ns()
{
  timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return (uint64_t)(t.tv_sec) * 1000000000 + t.tv_nsec;
}

#define I2C_SLAVE	0x0703
#define I2C_SMBUS	0x0720	/* SMBus-level access */

#define I2C_SMBUS_READ	1
#define I2C_SMBUS_WRITE	0

// SMBus transaction types
#define I2C_SMBUS_QUICK		    0
#define I2C_SMBUS_BYTE		    1
#define I2C_SMBUS_BYTE_DATA	    2 
#define I2C_SMBUS_WORD_DATA	    3
#define I2C_SMBUS_PROC_CALL	    4
#define I2C_SMBUS_BLOCK_DATA	    5
#define I2C_SMBUS_I2C_BLOCK_BROKEN  6
#define I2C_SMBUS_BLOCK_PROC_CALL   7		/* SMBus 2.0 */
#define I2C_SMBUS_I2C_BLOCK_DATA    8

// SMBus messages
#define I2C_SMBUS_BLOCK_MAX	32	/* As specified in SMBus standard */	
#define I2C_SMBUS_I2C_BLOCK_MAX	32	/* Not specified but we use same structure */

// Structures used in the ioctl() calls

union i2c_smbus_data
{
  uint8_t  byte;
  uint16_t word;
  uint8_t  block [I2C_SMBUS_BLOCK_MAX + 2];	// block [0] is used for length + one more for PEC
};

struct i2c_smbus_ioctl_data
{
  char read_write;
  uint8_t command;
  int size;
  union i2c_smbus_data *data;
};

static inline int i2c_smbus_access (int fd, char rw, uint8_t command, int size, union i2c_smbus_data *data)
{
  struct i2c_smbus_ioctl_data args;

  args.read_write = rw;
  args.command    = command;
  args.size       = size;
  args.data       = data;
  return ioctl(fd, I2C_SMBUS, &args);
}


int main() {
    MPU6050 mpu6050(0x68);
    mpu6050.calibrate();

    uint64_t start_time = get_time_ns();

    for (int i = 0;; i++) {
        int16_t x, y, z;
        // mpu6050.get_gyro_raw(&x, &y, &z);
        uint64_t st = get_time_ns();
        int status = ioctl(mpu6050.f_dev, I2C_SLAVE, 0x68);
        if (status < 0) {
          std::cout << "ERR (MPU6050.cpp:MPU6050())\n";
        }
        printf("%ld\n", get_time_ns() - st);

        union i2c_smbus_data data;
        data.block[0] = 6;
        if (i2c_smbus_access(mpu6050.f_dev, I2C_SMBUS_READ, 0x43, I2C_SMBUS_I2C_BLOCK_DATA, &data)) {
            printf("failed to read\n");
            return -1;
        } else {
            x = data.block[1] << 8 | data.block[2];
            y = data.block[3] << 8 | data.block[4];
            z = data.block[5] << 8 | data.block[6];
        }
            
        // mpu6050.get_gyro(&x, &y, &z);

        if (i % 100 == 0)
            printf("%d %d %d (rate: %f)\n", x, y, z, (i + 1) / ((get_time_ns() - start_time) / 1e9));
    }
    return 0;
}
