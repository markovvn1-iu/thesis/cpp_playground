#include <iostream>
#include <cstdio>
#include <ctime>
#include <cmath>

#include "mpu6050.h"

uint64_t get_time_ns()
{
  timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return (uint64_t)(t.tv_sec) * 1000000000 + t.tv_nsec;
}

double fast_sin(double x) {
    // e = lambda fps: 0.1 / (fps / 3.1415926 * 180 / 2000 * 32768)
    // scipy.optimize.minimize(lambda x: abs(abs(math.sin(x) - (x - x**3/6)) - e(100_000))  * 10000, 0.04)
    if (x > 0.04181492) return sin(x);
    // scipy.optimize.minimize(lambda x: abs(abs(math.sin(x) - (x)) - e(100_000))  * 100000, 0.001)
    if (x > 0.00185582) return x - x * x * x / 6;
    return x;

    // Check: max(math.sin(i / 1_000_000) - fast_sin(i / 1_000_000) for i in range(0, 1_000_000, 1)) < e(100_000)
}

double fast_cos(double x) {
    // e = lambda fps: 0.1 / (fps / 3.1415926 * 180 / 2000 * 32768)
    // scipy.optimize.minimize(lambda x: abs(abs(math.cos(x) - (1 - x**2/2)) - e(100_000))  * 100000000, 0.012)
    if (x > 0.01264496) return sin(x);
    // math.acos(1 - e(100_000))
    if (x > 4.6157652288863e-05) return 1 - x * x / 2;
    return 1;

    // Check: max(math.cos(i / 1_000_000) - fast_cos(i / 1_000_000) for i in range(0, 1_000_000, 1)) < e(100_000)
}

double safe_asin(double x)
{
	if (x > 1) return M_PI / 2;
	if (x < -1) return -M_PI / 2;

	return asin(x);
}

int main() {
    MPU6050 mpu6050(0x68);
    mpu6050.calibrate();

    double quaterion1[4] = {1, 0, 0, 0};
    double quaterion2[4];

    double* cur_quat = quaterion1;
	double* new_quat = quaterion2;

    uint64_t start_time = get_time_ns();
    uint64_t last_time = get_time_ns();

    for (int i = 0;; i++) {
        double x, y, z;
        mpu6050.get_gyro(&x, &y, &z);

        uint64_t cur_time = get_time_ns();
        double t = (cur_time - last_time) / 1e9;
        last_time = cur_time;

        double c1, c2, c3, s1, s2, s3;
		c1 = fast_cos(x * t / 2); s1 = fast_sin(x * t / 2);
		c2 = fast_cos(y * t / 2); s2 = fast_sin(y * t / 2);
		c3 = fast_cos(z * t / 2); s3 = fast_sin(z * t / 2);

        double delta_quat[4];
		delta_quat[0] = c1 * c2 * c3 - s1 * s2 * s3;
		delta_quat[1] = c1 * c2 * s3 + s1 * s2 * c3;
		delta_quat[2] = s1 * c2 * c3 + c1 * s2 * s3;
		delta_quat[3] = c1 * s2 * c3 - s1 * c2 * s3;

        new_quat[0] = (cur_quat[0] * delta_quat[0] - cur_quat[1] * delta_quat[1] - cur_quat[2] * delta_quat[2] - cur_quat[3] * delta_quat[3]);
		new_quat[1] = (cur_quat[0] * delta_quat[1] + cur_quat[1] * delta_quat[0] + cur_quat[2] * delta_quat[3] - cur_quat[3] * delta_quat[2]);
		new_quat[2] = (cur_quat[0] * delta_quat[2] - cur_quat[1] * delta_quat[3] + cur_quat[2] * delta_quat[0] + cur_quat[3] * delta_quat[1]);
		new_quat[3] = (cur_quat[0] * delta_quat[3] + cur_quat[1] * delta_quat[2] - cur_quat[2] * delta_quat[1] + cur_quat[3] * delta_quat[0]);

        double final_z = atan2(2.0 * (new_quat[0] * new_quat[1] + new_quat[2] * new_quat[3]), 1 - 2.0 * (new_quat[1] * new_quat[1] + new_quat[2] * new_quat[2]));
		double final_y = safe_asin(2.0 * (new_quat[0] * new_quat[2] - new_quat[3] * new_quat[1]));
		double final_x = atan2(2.0 * (new_quat[0] * new_quat[3] + new_quat[1] * new_quat[2]), 1 - 2.0 * (new_quat[2] * new_quat[2] + new_quat[3] * new_quat[3]));

        double* t_quat = new_quat;
        new_quat = cur_quat;
        cur_quat = t_quat;

        if (i % 100 == 0)
            printf("%f %f %f (rate: %f)\n", final_x / M_PI * 180, final_y / M_PI * 180, final_z / M_PI * 180, (i + 1) / ((get_time_ns() - start_time) / 1e9));
    }
    return 0;
}
