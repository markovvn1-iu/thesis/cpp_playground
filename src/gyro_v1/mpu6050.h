#include <cstdint>

class MPU6050 {
 public:
    int f_dev;

    void set_gyro_offset(int16_t x, int16_t y, int16_t z);
    void get_gyro_offset(int16_t* x, int16_t* y, int16_t* z);

    void get_gyro_raw(int16_t* x, int16_t* y, int16_t* z);

 public:
    MPU6050(uint8_t addr);

    void get_gyro(double* x, double* y, double* z);

    void get_accel_raw(int16_t* x, int16_t* y, int16_t* z);

    void calibrate(int iter = 1000);
};
