#include <iostream>
#include <cstdio>
#include <ctime>
#include <cmath>

#include "mpu6050.h"

uint64_t get_time_ns()
{
  timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return (uint64_t)(t.tv_sec) * 1000000000 + t.tv_nsec;
}

int main() {
    MPU6050 mpu6050(0x68);

    uint64_t start_time = get_time_ns();

    for (int i = 0;; i++) {
        int16_t x, y, z;
        mpu6050.get_accel_raw(&x, &y, &z);

        if (i % 100 == 0)
            printf("%d %d %d (rate: %f)\n", x, y, z, (i + 1) / ((get_time_ns() - start_time) / 1e9));
    }
    return 0;
}
