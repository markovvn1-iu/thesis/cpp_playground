#include "mpu6050.h"

#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <ctime>
#include <cmath>

extern "C" {
	#include <linux/i2c-dev.h>
	#include <i2c/smbus.h>
}

enum MPU6050_REG {
	MPU6050_RA_G_OFFS = 0x13,  // X_H, X_L, Y_H, Y_L, Z_H, Z_L
	MPU6050_RA_SMPLRT_DIV = 0x19,
	MPU6050_RA_CONFIG = 0x1A,
    MPU6050_RA_GYRO_CONFIG = 0x1B,
    MPU6050_RA_ACCEL_CONFIG = 0x1C,
	MPU6050_RA_ACCEL_OUT = 0x3b, // X_H, X_L, Y_H, Y_L, Z_H, Z_L
	MPU6050_RA_TEMP_OUT = 0x41, // H, L
	MPU6050_RA_GYRO_OUT = 0x43,  // X_H, X_L, Y_H, Y_L, Z_H, Z_L
	MPU6050_RA_PWR_MGMT_1 = 0x6B,
    MPU6050_RA_PWR_MGMT_2 = 0x6C,
};

MPU6050::MPU6050(uint8_t addr) {
	int status;

	f_dev = open("/dev/i2c-1", O_RDWR); //Open the I2C device file
	if (f_dev < 0) { //Catch errors
		std::cout << "ERR (MPU6050.cpp:MPU6050()): Failed to open /dev/i2c-1. Please check that I2C is enabled with raspi-config\n"; //Print error message
	}

	status = ioctl(f_dev, I2C_SLAVE, addr); //Set the I2C bus to use the correct address
	if (status < 0) {
		std::cout << "ERR (MPU6050.cpp:MPU6050()): Could not get I2C bus with " << addr << " address. Please confirm that this address is correct\n"; //Print error message
	}

	i2c_smbus_write_byte_data(f_dev, MPU6050_RA_PWR_MGMT_1, 0b00000000); //Take MPU6050 out of sleep mode - see Register Map

	i2c_smbus_write_byte_data(f_dev, MPU6050_RA_CONFIG, 0b00000000); //Set DLPF (low pass filter)

	i2c_smbus_write_byte_data(f_dev, MPU6050_RA_SMPLRT_DIV, 0b00000000); //Set sample rate divider - see Register Map

	i2c_smbus_write_byte_data(f_dev, MPU6050_RA_GYRO_CONFIG, 0b00011000); //Configure gyroscope settings - see Register Map (see MPU6050.h for the GYRO_CONFIG parameter)

	i2c_smbus_write_byte_data(f_dev, MPU6050_RA_ACCEL_CONFIG, 0b00010000); //Configure accelerometer settings - see Register Map (see MPU6050.h for the GYRO_CONFIG parameter)

	//Set offsets to zero
	i2c_smbus_write_byte_data(f_dev, 0x00, 0b10000001);
	i2c_smbus_write_byte_data(f_dev, 0x01, 0b00000001);
	i2c_smbus_write_byte_data(f_dev, 0x02, 0b10000001);
	i2c_smbus_write_byte_data(f_dev, 0x06, 0b00000000);
	i2c_smbus_write_byte_data(f_dev, 0x07, 0b00000000);
	i2c_smbus_write_byte_data(f_dev, 0x08, 0b00000000);
	i2c_smbus_write_byte_data(f_dev, 0x09, 0b00000000);
	i2c_smbus_write_byte_data(f_dev, 0x0A, 0b00000000);
	i2c_smbus_write_byte_data(f_dev, 0x0B, 0b00000000);
}

void MPU6050::set_gyro_offset(int16_t x, int16_t y, int16_t z) {
	uint8_t t[6] = {(uint8_t)(x >> 8), (uint8_t)(x & 0xff), (uint8_t)(y >> 8), (uint8_t)(y & 0xff), (uint8_t)(z >> 8), (uint8_t)(z & 0xff)};
	i2c_smbus_write_i2c_block_data(f_dev, MPU6050_RA_G_OFFS, 6, t);
}

void MPU6050::get_gyro_offset(int16_t* x, int16_t* y, int16_t* z) {
	uint8_t t[6];
	i2c_smbus_read_i2c_block_data(f_dev, MPU6050_RA_G_OFFS, 6, t);
	*x = t[0] << 8 | t[1];
	*y = t[2] << 8 | t[3];
	*z = t[4] << 8 | t[5];
}

void MPU6050::get_gyro_raw(int16_t* x, int16_t* y, int16_t* z) {
	uint8_t t[6];
	i2c_smbus_read_i2c_block_data(f_dev, MPU6050_RA_GYRO_OUT, 6, t);
	*x = t[0] << 8 | t[1];
	*y = t[2] << 8 | t[3];
	*z = t[4] << 8 | t[5];
}

void MPU6050::get_accel_raw(int16_t* x, int16_t* y, int16_t* z) {
	uint8_t t[6];
	i2c_smbus_read_i2c_block_data(f_dev, MPU6050_RA_ACCEL_OUT, 6, t);
	*x = t[0] << 8 | t[1];
	*y = t[2] << 8 | t[3];
	*z = t[4] << 8 | t[5];
}

void MPU6050::get_gyro(double* x, double* y, double* z) {
	int16_t xr, yr, zr;
	get_gyro_raw(&xr, &yr, &zr);

	if (abs(xr) < 6) xr = 0;
	if (abs(yr) < 6) yr = 0;
	if (abs(zr) < 6) zr = 0;

	double scale = 2000.0 / 32768.0 / 180.0 * M_PI;
	*x = xr * scale; *y = yr * scale; *z = zr * scale;
}

void MPU6050::calibrate(int iter) {
	while (true) {
		bool bad_data = false;
		int32_t x_sum = 0, y_sum = 0, z_sum = 0;
		int64_t x2_sum = 0, y2_sum = 0, z2_sum = 0;

		for (int i = 1; i <= iter; i++) {
			int16_t x, y, z;
			get_gyro_raw(&x, &y, &z);

			x_sum += x; y_sum += y; z_sum += z;
			x2_sum += (int32_t)x * x; y2_sum += (int32_t)y * y; z2_sum += (int32_t)z * z;
			usleep(1000);

			if (i % 100 == 0) {
				double x_std = sqrt((x2_sum * i - (int64_t)x_sum * x_sum)) / i;
				double y_std = sqrt((y2_sum * i - (int64_t)y_sum * y_sum)) / i;
				double z_std = sqrt((z2_sum * i - (int64_t)z_sum * z_sum)) / i;
				if (x_std > 3 || y_std > 3 || z_std > 3) {
					printf("Failed to calibrate\n");
					bad_data = true;
					break;
				}
			}
		}
		if (bad_data) continue;

		double x_mean = (double)x_sum / iter;
		double y_mean = (double)y_sum / iter;
		double z_mean = (double)z_sum / iter;

		double x_std = sqrt((x2_sum * iter - (int64_t)x_sum * x_sum)) / iter;
		double y_std = sqrt((y2_sum * iter - (int64_t)y_sum * y_sum)) / iter;
		double z_std = sqrt((z2_sum * iter - (int64_t)z_sum * z_sum)) / iter;

		if (x_std > 3 || y_std > 3 || z_std > 3) continue; // useless data
		// if (abs(x_mean) < 0.25 && abs(y_mean) < 0.25 && abs(z_mean) < 0.25) break; // done

		int16_t xo, yo, zo;
		get_gyro_offset(&xo, &yo, &zo);
		set_gyro_offset(xo - round(x_mean * 2), yo - round(y_mean * 2), zo - round(z_mean * 2));
		break;
	}
}