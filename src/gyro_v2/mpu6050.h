#include <cstdint>

class MPU6050 {
 private:
    int f_dev;

    void set_gyro_offset(int16_t x, int16_t y, int16_t z);
    void get_gyro_offset(int16_t* x, int16_t* y, int16_t* z);

    void get_gyro_raw(int16_t* x, int16_t* y, int16_t* z);

    void write_bit(uint8_t addr, uint8_t bit, bool value);

 public:
    MPU6050(uint8_t addr);

    void get_gyro(double* x, double* y, double* z);

    void get_accel_raw(int16_t* x, int16_t* y, int16_t* z);

    uint16_t get_fifo_count();

    void get_fifo_bytes(uint8_t* data, uint8_t len);

    void calibrate(int iter = 1000);

    void enable_fifo();
};
